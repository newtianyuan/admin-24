import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:8888/api/private/v1'

// 添加axios的请求拦截器
axios.interceptors.request.use(function (config) {
  // config:请求对象(内置参数)
  console.log(config)
  // 本地获取token
  let mytoken = localStorage.getItem('mytoken')
  if (mytoken) {
    // 需求：要把token存储到Authorization里, 这个字段是先与后台约定的
    config.headers.Authorization = mytoken
  }
  // 一定要返回这个对象
  return config
}, function (error) {
  // 返回错误信息的函数
  return Promise.reject(error)
})

// 发送登录请求
export const login = obj => axios.post('/login', obj).then(res => res.data)
// 获取用户数据列表
export const getUserList = obj => axios.get('/users', { params: obj }).then(res => res.data)
// 添加用户
export const addUser = obj => axios.post('/users', obj).then(res => res.data)
// 删除用户
export const deleteUser = id => axios.delete(`/users/${id}`).then(res => res.data)
// 修改用户状态
export const changeState = (id, type) => axios.put(`/users/${id}/state/${type}`).then(res => res.data)
// 编辑用户
export const editUser = (id, obj) => axios.put(`/users/${id}`, obj).then(res => res.data)
// 获取左侧权限菜单
export const getMenus = () => axios.get('/menus').then(res => res.data)
// 订单列表
export const getOrders = obj => axios.get('orders', { params: obj })

// 获取权限列表
export const getRightList = type => axios.get(`rights/${type}`).then(res => res.data)

// 获取角色列表
export const getRoles = () => axios.get('/roles').then(res => res.data)
// 分配用户角色
export const grantUserRole = (id, rid) => axios.put(`users/${id}/role`, { rid: rid }).then(res => res.data)
// 获取商品分类数据
export const getCate = (type, pagesize, pagenum) => axios.get('/categories', { params: { type: type, pagesize: pagesize, pagenum: pagenum } }).then(res => res.data)
// 添加分类
export const getaddCate = obj => axios.post('/categories', obj).then(res => res.data)
