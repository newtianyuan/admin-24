import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
    },
    {
      path: '/',
      name: 'home',
      // 按需加载，注释的作用是将这个home组件最后打包成一个以注释名字作为文件名的js文件
      component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
      redirect: { name: 'welcome' }, // 重定向：默认跳转welcome组件
      children: [
        {
          path: 'welcome',
          name: 'welcome',
          component: () => import(/* webpackChunkName: "welcome" */ '../views/Welcome.vue')
        },
        {
          path: 'users',
          name: 'users',
          // 按需加载 注释的作用是将这个login组件最后打包成一个以注释名为文件名的js文件
          component: () => import(/* webpackChunkName: "users" */ '../views/Users.vue')
        },
        {
          path: 'reports',
          name: 'reports',
          component: () => import(/* webpackChunkName: "reports" */ '../views/Reports.vue')
        }, {
          path: 'orders',
          name: 'orders',
          component: () => import(/* webpackChunkName: "orders" */ '../views/Orders.vue')
        }, {
          path: '/rights',
          name: 'rights',
          component: () => import(/* webpackChunkName: "rights" */ '../views/Rights.vue')
        },
        {
          path: '/roles',
          name: 'roles',
          component: () => import(/* webpackChunkName: "roles" */ '../views/Roles.vue')
        },
        {
          path: '/goods',
          name: 'goods',
          component: () => import(/* webpackChunkName: "goods" */ '../views/cate/Goods.vue')
        },
        {
          path: '/addgoods',
          name: 'addgoods',
          component: () => import(/* webpackChunkName: "addgoods" */ '../views/cate/AddGoods.vue')
        },
        {
          path: '/categories',
          name: 'categories',
          component: () => import(/* webpackChunkName: "categories" */ '../views/cate/CateGories.vue')
        }
      ]
    }
  ]
})
