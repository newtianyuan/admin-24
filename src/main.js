import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store'
import '@/styles/index.scss'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import ECharts from 'vue-echarts'
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.component('chart', ECharts)
Vue.use(VueQuillEditor)
// 添加一个全局路由守卫
router.beforeEach((to, from, next) => {
  let mytoken = localStorage.getItem('mytoken')
  // 如果已经存在token,那么所有页面都可以访问
  if (mytoken) {
    next()
  } else {
    to.name !== 'login' ? next('/login') : next()
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
